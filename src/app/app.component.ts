import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './services/authentication.service';
import { SignalRService } from './services/signalr.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Chat-Application-Assignment';
  showHeader: boolean;
 constructor(private authenticationService: AuthenticationService,
            private signalrService: SignalRService){}

  ngOnInit(){
    // when refreshed it updates the user's connectionId to databse
    if(localStorage.getItem('currentUser') !== null){
      this.signalrService.startConnection();
    }
  }
}
