import { Component, OnInit } from '@angular/core';
import { FormGroup , FormControl , Validators } from '@angular/forms';
import { signupviewModel } from '../Shared/user.service';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  signupForm: FormGroup;
  registerObect: signupviewModel ={
    userName: "",
    password: "",
    firstName: "",
    lastName: "",
    role: "",
    email: ""
  };
  constructor(private authService: AuthenticationService,
              private router: Router) { }

  ngOnInit() {
    this.signupForm = new FormGroup({
      'username': new FormControl(null, Validators.required),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'fname': new FormControl(null, Validators.required),
      'lname': new FormControl(null, Validators.required),
      'role': new FormControl(null, Validators.required),
      'password': new FormControl(null, Validators.required),
    });
    this.signupForm.patchValue({
      'role': 'User'
    });
  }

  onSignup(){
    this.registerObect.userName = this.signupForm.value.username;
    this.registerObect.email = this.signupForm.value.email;
    this.registerObect.firstName = this.signupForm.value.fname;
    this.registerObect.lastName = this.signupForm.value.lname;
    this.registerObect.role = this.signupForm.value.role;
    this.registerObect.password = this.signupForm.value.password;
    this.authService.Signup(this.registerObect)
      .subscribe(data =>{
        console.log(data);
        this.router.navigateByUrl('/home');
      },
      error => {
        console.log(error);
      });
  }
}
