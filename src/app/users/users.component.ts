import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { userDetail } from '../Shared/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  Users: userDetail[] = [];
  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.authenticationService.retreiveUsers()
        .subscribe((users: userDetail[]) => {
          this.Users = users;
          console.log(users);
        });
  }

}
