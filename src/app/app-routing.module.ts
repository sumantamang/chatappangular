import { NgModule } from '@angular/core';
import { Routes , RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { UsersComponent } from './users/users.component';
import { AuthGuard } from './services/authguard.service';
import { MessagesComponent } from './messages/messages.component';
import { ConversationComponent } from './messages/conversation/conversation.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';

const appRoutes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full'},
    { path: 'login', component: LoginComponent},
    { path: 'home', component: HomeComponent, canActivate:[AuthGuard], data:{role:['Admin','User']}},
    { path: 'register', component: RegisterComponent, canActivate:[AuthGuard], data:{role:['Admin']}},
    { path: 'users', component: UsersComponent, canActivate:[AuthGuard], data:{role:['Admin']}},
    { path: 'forbidden', component: ForbiddenComponent},
    { path: 'messages', component: MessagesComponent, canActivate:[AuthGuard], data:{role:['User','Admin']}, children: [
       { path: 'conversations/:id', component: ConversationComponent, canActivate:[AuthGuard], data:{role:['User','Admin']}}
    ]}
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule{

}