import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { userDetail, signupviewModel } from '../Shared/user.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild ('f', { static: false }) signupForm: NgForm;
  userDetail: userDetail={
    userName: "",
    firstName: "",
    lastName: "",
    role: "",
    email: "",
    id: ""
  };
  x: string[] = ["f","f","d","d","d","d","f","d","d"];
  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.authenticationService.RetreiveUserData(localStorage.getItem('userName')) 
        .subscribe((userData: userDetail) => {
          this.userDetail = userData;
          console.log(userData);
        });
  }
  
  getColor(n: number){
    return n%2 === 0 ? 'blue' : 'green';
  }
  givewhat(n: number){
    return n%2 === 0 ? true : false;
  }
  send(){
    this.x.push("hhh");
  }
}

