export interface loginviewModel{
     Username: string;
     Password: string;
}

export interface signupviewModel{
    userName: string;
    password: string;
    firstName: string;
    lastName: string;
    role: string;
    email: string;
}
export class HubRepository {
    constructor(
        public ConnectionId: string,
        public UserName: string
    ){}
}
export interface messageModel {
    messageId: number;
    senderFirstName: string;
    receiverUserName: string;
    content: string;
    timeStamp: string;
    senderUserName: string;
}
export interface userDetail {
    userName: string;
    id: string;
    firstName: string;
    lastName: string;
    role: string;
    email: string;
}

