import { Component, OnInit, ViewChild, ElementRef, AfterContentChecked, AfterViewChecked } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { messageModel } from 'src/app/Shared/user.service';
import { SignalRService } from 'src/app/services/signalr.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import {  NgForm } from '@angular/forms';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit , AfterViewChecked{

  receiverName: string;
  messageContent: string;
  conversationTable: string;
  messageObejct: messageModel = {
    messageId: 0,
    senderFirstName: "",
    senderUserName: "",
    content: "",
    timeStamp: "",
    receiverUserName: ""
  }
  receivedMessages: messageModel[] = [];
  rm: string[] = [];
  isLoading: boolean = false;
  currentUser: string = localStorage.getItem('userName').toString();
  messageArea: string = "";
  @ViewChild('f', {static: false}) messageForm: NgForm;
  @ViewChild('scroll', {static: false}) myScrollContainer: ElementRef;
  constructor(private route: ActivatedRoute,
              private signalrService: SignalRService,
              private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.autoScroll();
    this.isLoading = true;
    this.route.params
      .subscribe((Params: Params) => {
          this.receiverName = Params['id'];
          this.conversationTable = localStorage.getItem('userName') + this.receiverName;
          this.authenticationService.RetreiveMessages(this.receiverName, localStorage.getItem('userName').toString())
          .subscribe((message: messageModel[])=> {
            console.log(message);
            this.receivedMessages = message;
            this.isLoading = false;
          }),() => {
            this.isLoading = false;
          };
      }), () => {
        this.isLoading = false;
      };
   
    this.signalrService.receivedMessage
        .subscribe((message: messageModel) => {
          console.log("inside conversation component");
           console.log(message);
           if(this.receiverName === message.senderUserName || this.receiverName === message.receiverUserName){
            this.receivedMessages.push(message);
            this.autoScroll();
           }
        });
  }
  ngAfterViewChecked(){
    this.autoScroll();
  }
  sendMsg(){
    this.messageObejct.receiverUserName = this.receiverName;
    this.messageObejct.content = this.messageContent;
    this.messageObejct.senderUserName = localStorage.getItem('userName');
    console.log(this.messageObejct);
    this.signalrService.sendMessage(this.messageObejct);
    this.messageArea = "";
  }
  getColor(senderName: string){
    return senderName === this.currentUser ? 'blue' : 'green';
  }
  givewhat(senderName: string){
    return senderName === this.currentUser ? true : false;
  }
  onKeyPressed(){
    this.messageObejct.receiverUserName = this.receiverName;
    this.messageObejct.content = this.messageForm.value.usermsg;
    this.messageObejct.senderUserName = localStorage.getItem('userName');
    this.signalrService.sendMessage(this.messageObejct);
    this.messageForm.reset();
  }
  autoScroll(){
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    }
    catch(error){}
  }
}
