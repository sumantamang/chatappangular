import { Component, OnInit } from '@angular/core';
import { MessageService } from '../services/messages.service';
import { signupviewModel, userDetail } from '../Shared/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  users: userDetail[] = [];
  loading: boolean = false;
  constructor(private messageService: MessageService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.loading = true;
    this.messageService.getUsers()
        .subscribe( (users: userDetail[]) => {
          console.log(users);
          this.users = users;
          this.loading = false;
          this.router.navigate(["conversations/"+this.users[0].userName], {relativeTo: this.route});
        });
  }
  userClicked(user: userDetail){
    this.router.navigate(["conversations/"+user.userName], {relativeTo: this.route});
  }

}
