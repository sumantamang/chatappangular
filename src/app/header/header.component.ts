import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { SignalRService } from '../services/signalr.service';
import { AuthenticationService } from '../services/authentication.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  allowFunc: boolean = false;
  userName: string = "";
  constructor(private router: Router,
              private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.authenticationService.isLoggedIn
        .subscribe(() => {
          this.allowFunc = true;
          this.userName = localStorage.getItem("userName");
        });
    if(localStorage.getItem('currentUser') !== null){
      this.allowFunc = true;
      this.userName = localStorage.getItem("userName");
    }
    
  }
  onLogOut(){
    this.authenticationService.LogOut();
    this.allowFunc = false;
    this.router.navigate(['login']);
  }

}
