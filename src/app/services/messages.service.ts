import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { userDetail } from '../Shared/user.service';

@Injectable({
    providedIn: 'root'
  })
export class MessageService{
    apiUrl: string = "https://localhost:44381/api";
    constructor(private httpClient: HttpClient){}

    getUsers(){
      return  this.httpClient.get(this.apiUrl+'/messages/getusers');         
    }        
}
