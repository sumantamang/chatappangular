import { Injectable, Output } from '@angular/core';
import * as signalR from "@aspnet/signalr";
import { signupviewModel, HubRepository, messageModel } from '../Shared/user.service';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SignalRService{

    private hubConnection: signalR.HubConnection;
    @Output() connectionIds = new Subject<HubRepository[]>();
    @Output() receivedMessage = new Subject<messageModel>();
    public startConnection = () => {
        this.hubConnection = new signalR.HubConnectionBuilder()
                                        .withUrl('https://localhost:44381/chat')
                                        .build();
        this.hubConnection
            .start()
            .then(() => console.log('Connection Started'))
            .catch(err => console.log('Error while starting connection: '+err));
        this.hubConnection.on("onConnected", () => {
            this.hubConnection.invoke("SendUserData", localStorage.getItem('userName'));
        this.hubConnection.on("receiveMessage",(messageObj: messageModel) => {
            console.log("receiving (inside signalr)");
            console.log(messageObj);
            this.receivedMessage.next(messageObj);
        })
        });
    }
    public getConnectionId = () => {
        this.hubConnection.on("ConnectionIds", (connectionIds: HubRepository[]) => {
            console.log(connectionIds);
            this.connectionIds.next(connectionIds);
        });
    }
    public stopConnection = () => {
        this.hubConnection.stop();
        console.log("stopped connection");
    }
    public sendMessage(messageObj: messageModel){
        console.log("sending"+messageObj);
        this.hubConnection.invoke("Sendmessage", messageObj)
            .catch(err => console.log(err));
    }
    public receiveMessage(){
        this.hubConnection.on("receiveMessage",(messageObj: messageModel) => {
            console.log("receiving "+messageObj);
            this.receivedMessage.next(messageObj);
        })
    }
 

}