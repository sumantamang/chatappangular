import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Injectable, Output } from '@angular/core';
import {  signupviewModel, messageModel, userDetail } from '../Shared/user.service';
import { Subject } from 'rxjs';
import { SignalRService } from './signalr.service';

@Injectable({
    providedIn: 'root'
  })
export class AuthenticationService{
    apiUrl: string = "https://localhost:44381/api";
    apiUrlMessage: string = "https://localhost:44381/api/messages";
    @Output() isLoggedIn = new Subject<boolean>();

    constructor(private http: HttpClient,
                private signalrService: SignalRService) { }

    Login(username: string, password: string){
        return this.http.post<any>(this.apiUrl+'/userCredential/login', { username, password})
            .pipe(map(user => {
                console.log(user);
                if(user && user.token){
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    localStorage.setItem('userName',username);  
                    this.isLoggedIn.next(true);
                }
                else {
                    console.log(user.message);
                    return user;
                }
            }));
    }
    LogOut(){
        localStorage.removeItem('currentUser');
        localStorage.removeItem('userName');
        this.signalrService.stopConnection();
    }
    Signup(signUpUser: signupviewModel){
        return this.http.post<any>(this.apiUrl+'/userCredential/create', signUpUser)
            .pipe(map(res => {
                console.log(res);
            }));
    }
    RetreiveUserData(userName: string){
        return this.http.get<userDetail>(this.apiUrl+'/UserCredential/retreiveUser/'+userName );
    }
    RetreiveMessages(u1:string, u2 : string){
        console.log("retreiving messages.");
        return this.http.get<messageModel[]>(this.apiUrlMessage +'/getmessage/'+u1+'/'+u2);
    }
    matchRole(allowedRoles): boolean {
       var isMatched = false;
       var payLoad = JSON.parse(window.atob(localStorage.getItem('currentUser').split('.')[1]));
       var userRole = payLoad.role;
       allowedRoles.forEach(element => {
           console.log("element "+element+ userRole);
           if(userRole === element){
               isMatched = true;
               //return false;
           }
       });
        return isMatched;
    }
    retreiveUsers(){
        return this.http.get<userDetail[]>(this.apiUrl+"/UserCredential/retreiveUsers");
    }
}