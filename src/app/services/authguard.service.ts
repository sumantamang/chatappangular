import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { Injectable } from '@angular/core';
@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate{

    constructor(private router: Router,
                private authenService: AuthenticationService){

    }
    canActivate(
        next: ActivatedRouteSnapshot,
        ): boolean {
            if (localStorage.getItem('currentUser') != null){
                let roles = next.data['role'] as Array<string>;
                if(roles){
                    if(this.authenService.matchRole(roles)){
                        return true;
                    }
                    else {
                        this.router.navigate(['/forbidden']);
                        return false;
                    }
                }
                this.router.navigate(['/forbidden']);
                return false;
            }
            else {
                this.router.navigate(['/forbidden']);
                return false;
            }
        }
    
}