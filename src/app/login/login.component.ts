import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';
import { Router, Route } from '@angular/router';
import { first } from 'rxjs/operators';
import { SignalRService } from '../services/signalr.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  error: string ="";
  loading = false;
  constructor(private authService: AuthenticationService,
              private router: Router,
              private signalrService: SignalRService) { }

  ngOnInit() {
    if(localStorage.getItem('userName') != null){
      this.router.navigateByUrl('/messages');
    }
    this.loginForm = new FormGroup({
      'username': new FormControl(null, Validators.required),
      'password': new FormControl(null, Validators.required)
    });
  }
  onSubmit(){
    this.loading = true;
    this.authService.Login(this.loginForm.value.username, this.loginForm.value.password)
      .pipe(first())
      .subscribe(
        () => { 
          this.router.navigateByUrl('/messages');
          this.signalrService.startConnection();
          this.signalrService.getConnectionId();
          this.loading = false;
        },
        error => {
          this.error = "Your Username or Password is wrong";
          console.log(error.message);
          this.loading = false;
        }
      )
  }
  
}
